const http = require ('http');

const port = 4000;
//create a variable 'port'  to store the port number

const server = http.createServer( (request, response) => {
// '/greeting' - endpoint that returns a message "Greeting Page"
    if (request.url == '/greeting'){
        //url - it is a property that refers to the url or the link to the browser
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Greeting Page');
    }
   
    else if (request.url == '/homepage'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('This is my homepage');
    }

    else {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end('Page Unavailable');
    }
});

server.listen(port);
console.log(`Server now accessible at localhost: ${port}`);

//how to run? nodemon routes.js
//how to access the endpoint? http://localhost:4000/<endpoint>