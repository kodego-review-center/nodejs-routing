//Step1: NodeJS Installation
//link: https//nodejs.org/en
//recommend using LTS (Long Time Support) because the software release is maintained for extended period of time

//Step2: Create the require() function
// allows us to use built-in methods of NodeJS modules.
//similar <script></script>
let http = require ('http');

//Step3: Use createServer() method
//we can create HTTP server that listens to request on a specified port and gives back response to the client.
http.createServer( function (request,response) {
//request - messages sent by the client
//response - answer that was sent by the server
//Clients (FE) and servers (NodeJS, expressJS application) communicate by exchanging individual messages


response.writeHead(200, {'Content-Type': 'text/plain'});
//writeHead() - sets the status code for the response 200 (which means OK)
//set the content type as a plain text message

response.end('Hello Jericho');
//response.end() - terminates the response process


}).listen(4000); //the server will be assigned to port via the method listen

console.log(`Server is running at localhost 4000`);